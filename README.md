#   RPCS3 Auto Downloader (**UNOFFICIAL**)

##  How to install
    - pip3 install -r requirements.txt --user

## How to use (Linux)
    - chmod +x runme.py
    - ./runme.py

## How to use (Windows)
    - make sure you have python3 installed
    - double click the runme.py
    
## With Destination Argument (Both)
    - runme.py --path [location of destination]
