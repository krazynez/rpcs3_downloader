#!/usr/bin/env python3

# Author: Brennen Murphy
# Date: 11/16/2021
# Contact me: BrennenMurph@pm.me
# Or the discord server: https://discord.gg/pAh6Cpv

from sys import platform
import requests
from bs4 import BeautifulSoup
import re, wget, argparse, os, glob
from pyunpack import Archive

def download_rpcs3(p=None):
    file_path=p.path
    file_path=str(file_path).replace("'", "").replace("]", "").replace("[", "")
    url = 'https://rpcs3.net/download'
    website = requests.get(url, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:104.0) Gecko/20100101 Firefox/104.0'})
    #html = urlopen(website).read().decode('utf-8')
    soup = BeautifulSoup(website.text, 'html.parser')
    #links = re.findall(r"'https://github.com/.*?'", html)

    if platform == 'linux' or platform == 'linux2':
        os.system('clear')
        print('\n-- Linux Support Was Found --\n Downloading latest RPCS3 now!\n')

        for link in soup.find_all('a'):
            if 'AppImage' in link.get('href') and p.path is None:
                new_filename=link.get('href').split('/')[-1]
                if os.path.exists(new_filename):
                    print('# -> Lastest already available <- #')
                    return
                print(' Saving RPCS3 to:', os.getcwd(), '\n')
                rpcs3 = wget.download(link.get('href'))
                _rpcs3 = glob.glob('rpcs3*AppImage')
                os.chmod(''.join(_rpcs3), 0o755)
                return

            elif 'AppImage' in link.get('href') and p.path is not None:
                new_filename=link.get('href').split('/')[-1]
                if os.path.exists(new_filename):
                    print('# -> Lastest already available <- #')
                    return
                print(' Saving RPCS3 to: ' + file_path + wget.filename_from_url(link.get('href')) + '\n')
                rpcs3 = wget.download(link.get('href'))
                _rpcs3 = glob.glob(f'{file_path}/rpcs3*AppImage')
                os.chmod(''.join(_rpcs3), 0o755)
                return

        

    elif platform == 'win32':
        os.system('cls')
        print('\n-- Windows Support Was Found --\n Downloading latest RPCS3 now!\n')
        for link in links:
           
            if '7z' in link and p.path is None:
                print(' Saving RPCS3 to: ' + os.getcwd(), "\n")
                rpcs3 = wget.download(link.replace("'",""))
                print("\n", wget.filename_from_url(link.replace("'","")))
                filename=wget.filename_from_url(link.replace("'",""))
                if not os.path.isdir(f'{os.getcwd()}/rpcs3'):
                    os.mkdir('rpcs3', mode=0o755)
                Archive(f'{filename}').extractall(f'{os.getcwd()}/rpcs3')
                os.remove(f"{filename}")
                return

            elif '7z' in link and p.path is not None:
                filename=wget.filename_from_url(link.replace("'","")) 
                print(' Saving RPCS3 to: ' + file_path + '/' +filename + '\n')
                full_file_path=file_path + '/' + filename
                rpcs3 = wget.download(link.replace("'", ""), file_path)
                if not os.path.isdir(f'{file_path}/rpcs3'):
                    os.mkdir(f'{file_path}/rpcs3')
                Archive(f"{full_file_path}").extractall(f"{file_path}/rpcs3")
                os.remove(f"{full_file_path}")
                return

    else:
        print('OS not supported or added yet!')
        return
                

def main():
    parser = argparse.ArgumentParser(description='Add Path to where you want to download RCPS3 to')
    parser.add_argument('-p', '--path', type=str, nargs='*', help='path to download and save RPCS3')
    args = parser.parse_args()

    try:
        download_rpcs3(args)
    except ValueError as e:
        print('\nSomthing went wrong!\n')
        print(f'ERROR: {e}\n')
    finally:
        print('\n\nEnjoy RPCS3\n')
        if args.path is not None and platform == 'win32':
            print(f"Location: {''.join(args.path)}/rpcs3\n")
        if args.path is not None and platform == 'linux' or platform == 'linux2':
            print(f"Location: {''.join(args.path)}\n")



if __name__ == '__main__':
    main()
