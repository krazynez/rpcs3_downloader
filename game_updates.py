#!/usr/bin/env python3
import requests, sys, re
from urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

if len(sys.argv) < 2:
    print("You need to add the GAME ID also: ./game_updates.py <GAMEID>")
    sys.exit(1)

GAMEID = sys.argv[1]

URL = f"https://a0.ww.np.dl.playstation.net/tpl/np/{GAMEID}/{GAMEID}-ver.xml"


resp = requests.get(URL, verify=False)


res = re.findall(r'\w+://[^\s]+', resp.text)

for urls in res:
    print(urls.replace('"', ''))
